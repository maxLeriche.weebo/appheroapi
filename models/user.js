var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var log=require('../Utils/Log')
const fs = require('fs');
let fichier = fs.readFileSync(__dirname + '/BDDINFO.weebconfig');
let configfile = JSON.parse(fichier);
const BDDINFO =configfile.BDDlocalhost;

mongoose.connect(BDDINFO, {useNewUrlParser: true})
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  log.info("We are connected");
});
var LOGIN = new Schema({
    pseudo: String,
    mail: String,
    password: String,
    nom:String,
    prenom:String,
    birthdate: Date,
    preference:{}
});

var uttilisateur = mongoose.model('LOGIN',LOGIN);
//max = new uttilisateur({pseudo:"Batleforc",mail:"maxime.leriche@weebo.fr",nom:"leriche",prenom:"Maxime"})
//max.save(function(errr){if(errr) return console.log("sa save passs")});
log.info("User ready");
exports.default = uttilisateur;
