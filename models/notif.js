var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var log=require('../Utils/Log')
const fs = require('fs');
let fichier = fs.readFileSync(__dirname + '/BDDINFO.weebconfig');
let configfile = JSON.parse(fichier);
const BDDINFO =configfile.BDDlocalhost;

mongoose.connect(BDDINFO, {useNewUrlParser: true})
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  log.info("We are connected");
});
var notif = new Schema({
    subscription:String
});

var uttilisateur = mongoose.model('notif',notif);

log.info("notif ready");
exports.default = notif;
