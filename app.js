var express = require('express');
var log=require('./Utils/Log')
var fs = require('fs');
var https = require('https');
var app = express();
var apirouter =require('./apiRouter').router;
server = https.createServer({
  key: fs.readFileSync('./privkey.pem'),
  cert: fs.readFileSync('./fullchain.pem')
}, app);
log.info('Initialisation du programe');
var CHAT = require('./routes/chatctrl');
var test = new CHAT(server);

app.use(function (req, res, next) {

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'https://api.weebo.fr');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Pass to next layer of middleware
  next();
});

app.use(express.json());

app.get('/', function (req, res) {
  res.send('APPHERO (API MADE WITH A LOT OF COFEE) From: Batleforc');
  log.info("appelle de la racine de l'api");
});


app.use('/',apirouter);


server.listen(25570, function () {
  log.info('API listening on port 25570! Go to https://localhost:25570/ and on https://api.weebo.fr/');
});
