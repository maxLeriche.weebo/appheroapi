var bcrypt   = require('bcrypt');
var jwtUtils = require('../Utils/jwt.utils');
var test = require('../models/user');
var mongoose = require('mongoose');
var user=mongoose.model('LOGIN');
var log=require('../Utils/Log')

var validation = async function(mail,pass,pseudo){
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (pass.length<6||!re.test(String(mail).toLowerCase())||pseudo.length<=3)
    {
        return true;
    }
    return false;
};

module.exports ={
    register : async function(req,res){
        var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        var mail = req.body.mail;
        var password = req.body.pass;
        var pseudo =req.body.pseudo;
        
        if (mail==null||password==null||pseudo==null||await validation(mail,password,pseudo))
        {
            log.info(`REGISTER:${ip}: ${pseudo} error 400 missing or invalid parameters`);
            return res.status(400).json({'error':'missing or invalid parameters'});
        }
       try{
        user.findOne({mail:mail}).exec(function (err, adventure) {
            if(err) 
            {
                log.info(`REGISTER: ${ip}:${pseudo} error 500 unable to verify user `);
                return res.status(500).json({'error':'Unable to verify user'});
            }
            if(adventure) 
            {
                log.info(`REGISTER: ${ip}:${pseudo} error 409 user already exist`);
                return res.status(409).json({'error':'User already exist'});
            }
         });
         bcrypt.hash(password,5,function(err,bcryptedpassword){
             max = new user({pseudo:pseudo,mail:mail,nom:req.body.nom,prenom:req.body.prenom,password:bcryptedpassword})
             });
        await max.save(function(errr,user){
             if(errr){
                log.info(`REGISTER: ${ip}:${pseudo} error 409 unable to save the user`);
                 return res.status(409).json({'error':'Unable to save the user'});
             }
             log.info(`REGISTER: ${ip}:${pseudo} 201 ${user._id}`);
             return res.status(201).json({'userID':user._id});
         });
       }
        catch(e)
         {
            log.error(`REGISTER:${ip}: ${pseudo} error 500 ${e}`);
             return res.status(500).json({'error':e})
         }
    },
    login:async  function(req,res){
        var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        var mail = req.body.mail;
        var pass = req.body.pass;

        if(mail==null||pass==null&&await validation(mail,pass,"jesuisunecruche")){
            log.info(`LOGIN:${ip}: ${mail} error 400 missing or invalid parameters`);
            return res.status(400).json({'error':'missing parameters or incorect format'});
        }
        try
        {
           await user.findOne({mail:mail}).exec(async function (err, adventure) {
                if(err){
                    log.info(`LOGIN: ${ip}: error 500 unable to verify user`);
                     return res.status(500).json({'error':'Unable to verify user'});
                }
                if(adventure)
                {
                    return  await bcrypt.compare(pass,adventure.password,async function(errBcrypt,resBcrypt){
                        if(resBcrypt){
                            log.info(`LOGIN:${ip}:  200 user loged in with id ${adventure._id}  `);
                            return res.status(200).json({
                                'userId':adventure._id,
                                'token': await (jwtUtils.generateRenewTokenForUser(adventure))
                            });
                            
                        }
                        else{
                            log.info(`LOGIN: ${ip}:error 403 combo user password doesnt work `);
                            return res.status(403).json({'error':'combo user password doesnt work'});}
                    });
                }
                log.info(`LOGIN: ${ip}: error 404 user not exist`);
                return res.status(404).json({'error':'user not exist'});
             });
        }
         catch(e)
         {
            log.error(`LOGIN: ${ip}:${pseudo} error 500 ${e}`);
             return res.status(500).json({'error':e})
         }
    },
    getuser:async function(req,res){
        var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        var headerAuth = req.headers['authorization'];
        var userID =await jwtUtils.getUserId(headerAuth);
        if (userID<0)
        {
            log.info(`GETUSER: ${ip}:${userID} error 400 WRONG TOKEN`);
            return res.status(400).json({'error': 'WRONG TOKEN'});
        }
           
        try
        {
            await user.findOne({_id:userID}).exec(async function (err, adventure) {
                if(err) 
                {
                    log.info(`GETUSER:${ip}: ${userID} error 500 unable to verify user`);
                    return res.status(500).json({'error':'Unable to verify user'});
                }
                if(adventure)
                {
                    log.info(`GETUSER:${ip}: ${userID} error 201 user found and returned`);
                    return res.status(201).json(adventure);
                }
                log.info(`GETUSER: ${ip}:${userID} error 404 user not found`);
                return  res.status(404).json({'error':'user not found'});
                });
        }
        catch(e)
        {
            log.info(`GETUSER:${ip}: ${userID} error 500 ${e}`);
            return res.status(500).json({'error':e})
        }
    },
    updateUser:async function(req,res){
             var ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
            var headerAuth = req.headers['authorization'];
            var userID =await jwtUtils.getUserId(headerAuth);
            var preference = req.body.preference;
            var birthdate = req.body.birthdate;
            if (userID<0)
            {
                log.info(`UPDATEUSER:${ip}: ${userID} error 400 WRONG TOKEN`);
                return res.status(400).json({'error': 'WRONG TOKEN'});
            }
           
            try
            {
                user.findOne({_id:userID}).exec(async function (err, adventure) {
                    if(err) 
                    {
                        log.info(`UPDATEUSER:${ip}: ${userID} error 500 unable to verify user`);
                        return res.status(500).json({'error':'Unable to verify user'});
                    }
                    if(adventure){
                        adventure.preference =preference?preference : adventure.preference;
                        adventure.birthdate =birthdate?birthdate : adventure.birthdate;
                        await adventure.save();
                        log.info(`UPDATEUSER: ${ip}:${userID} error 201 user updated`);
                        return res.status(201).json(adventure);
                    }
                    log.info(`UPDATEUSER: ${ip}:${userID} error 404 user not found`);
                    return  res.status(404).json({'error':'user not found'});
                 });
            }
            catch(e)
            {
                log.info(`UPDATEUSER: ${ip}:${userID} error 500 ${e}`);
                return res.status(500).json({'error':e})
            }
    },
    getaccessToken: async function(req,res){
         var headerAuth = req.headers['authorization'];
        var userID =await jwtUtils.getUserId(headerAuth);
        if(await jwtUtils.validateRenewTokenForUser(headerAuth)<0){
            var adventure = {_id:await jwtUtils.getUserIdFromRenew(headerAuth),pseudo:await jwtUtils.getPseudoFromRenew(headerAuth)}
            return res.status(200).json({
                'token': await (jwtUtils.generateTokenForUser(adventure))
            });
        }
    }
};

