var jwtUtils = require('../Utils/jwt.utils');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var log=require('../Utils/Log')
var message = new Schema({
    pseudo: String,
    Content:String,
    date:String
});

class chat
{
    constructor(https)
    {
        this.server=https;
        this.io = require('socket.io')(this.server, { secure: true});
        this.histomaxgenerale=30;
        this.histomaxother=15;
        this.histo={};
        this.user={};
        this.Chan={};
        this.io.on('connection', (socket) => {
            console.log("Bas j'ai une connection");
          });
          this.user.generale=Array();
          this.histo.generale=mongoose.model('generalemsg',message);
          this.Chan.generale=this.io.of('/chat/').on('connection',async function(socket){
            if(this.user.generale.includes())
            socket.on('INIT',async  function(msg){
                var userPseudo =await jwtUtils.getUserId(msg.token);
            })
            socket.on('message',async  function(msg){
                if(this.user.generale.hasOwnProperty(socket)){

                }else{
                    socket.emit('log', {status:"Error",message:"User tryed to connect with an invalid token"});
                }
            })
            socket.on('log',async function(msg){
                if(this.user.generale.hasOwnProperty(socket)){

                }else{
                    socket.emit('log', {status:"Error",message:"User tryed to connect with an invalid token"});
                }
            })
            socket.on('disconnect', async function(){
                if(this.user.generale.hasOwnProperty(socket)){

                }else{
                    socket.emit('log', {status:"Error",message:"User tryed to connect with an invalid token"});
                }
            })
        })
    }
    async sendAnnonce(msg)
    {
        this.Chan.generale.emit("annonce",msg);
    }
    async newchan(req,res)
    {
        var headerAuth = req.headers['authorization'];
        var userID = await jwtUtils.getUserId(headerAuth);

        if (userID<0)
             return res.status(400).json({'error': 'WRONG TOKEN'});
        if(!req.params.hasOwnProperty("chan"))
        {
            return res.status(400).json({'error':'Please specify a chan name'})
        }
        if(!this.Chan.hasOwnProperty(req.params.chan))
        {
            return res.status(400).json({'error':'CHAN ALREADY CREATE'});
        }
        this.chan[req.params.chan]=this.io.of(`/chat/${req.params.chan}`).on('connection')
        this.user[req.params.chan]=Array();
        this.histo[req.params.chan]=Array();
    }
}


module.exports = chat;