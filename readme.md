# AppHeroAPI

AppheroAPI est l'interface Serveur lier aux projet APPHERO et fonctionne avec AppHeroUI.

## Techno

NodeJS,ExpressJS,Mongoose,Socket.io,JWT

## Usage

```shel
npm install

npm start

```

une fois fais go en localhost:... Tout dépend

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## REPO PUBLIC

[REPO](https://gitlab.com/maxLeriche.weebo/appheroapi) [Make MD](https://www.makeareadme.com/#mind-reading)
