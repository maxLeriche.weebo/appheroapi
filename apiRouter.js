var express = require('express');
var UsrCtrls = require('./routes/UsrCtrl');
var NotifCtrls = require('./routes/NotifCtrl');

exports.router =(function(){
    var apirouter = express.Router();

    apirouter.route('/acc/register').post(UsrCtrls.register);
    apirouter.route('/acc/login').post(UsrCtrls.login);
    apirouter.route('/acc/me').get(UsrCtrls.getuser);
    apirouter.route('/acc/me').put(UsrCtrls.updateUser);
    apirouter.route('/acc/Token').post(UsrCtrls.getaccessToken);
    apirouter.route('/notif/subscrive').post(NotifCtrls.subscription);
    
    apirouter.route('/chats').get((req,res)=>{
        res.sendFile(__dirname + '/simu/test.html');
    })
    apirouter.route('/chat/:chan').get((req,res)=>{
       console.log("Someone's listening to "+req.params.chan);
       res.status(200).json({'status': 'cest good bro'});
    })
    apirouter.route('/chat/:chan/:message').get((req,res)=>{
        console.log("Someone's trying to delete  "+req.params.chan+":"+req.params.message);
        res.status(200).json({'status': 'cest good bro'});
     })
 

    return apirouter;
})();