var jwt = require('jsonwebtoken');
const fs = require('fs');
var bcrypt   = require('bcrypt');
let fichier = fs.readFileSync(__dirname + '/test.weebconfig');
let configfile = JSON.parse(fichier);
const JWT_SIGN_SECRET =configfile.jwtsign;
const renew_jwt_sign = configfile.jwtrenewsign;

module.exports ={
  refreshTokens: {},
    generateTokenForUser :async function(userData){
        return jwt.sign({
            userID:userData._id,
            userPseudo:userData.pseudo
        }, JWT_SIGN_SECRET,
        {
            expiresIn: '2h'
        })
    },
    validateRenewTokenForUser: async function(authorization){
      var userId = -1;
      var token =  await module.exports.parseAuthorization(authorization);
      if(token != null&&(token in module.exports.refreshTokens)) {
        try {
          var jwtToken =await jwt.verify(token, renew_jwt_sign);
          if(jwtToken != null)
            {
              userId = jwtToken.userID;
            }
        } catch(err) { }
      }
      return userId;
    },
    generateRenewTokenForUser: async function(userData){
      await bcrypt.hash(userData._id,5,function(err,bcryptedpassword){
        var swap= jwt.sign({
          type:"Refresh Token",
          userID:userData._id,
          userPseudo:userData.pseudo,
          token:bcryptedpassword
  
        },renew_jwt_sign,{
          expiresIn:'14d'
        })
        console.log(swap);
        console.log(module.exports.refreshTokens)
        module.exports.refreshTokens[userData._id] = swap;
        });
      
      return  module.exports.refreshTokens[userData._id];
    },
    parseAuthorization: async function(authorization) {
        return (authorization != null) ? authorization.replace('Bearer ', '') : null;
      },
    getUserId:  async function(authorization) {
        var userId = -1;
        var token =  await module.exports.parseAuthorization(authorization);
        if(token != null) {
          try {
            var jwtToken =await jwt.verify(token, JWT_SIGN_SECRET);
            if(jwtToken != null)
              userId = jwtToken.userID;
          } catch(err) { }
        }
        return userId;
      },
    getPseudo: async function(authorization){
        var pseudo = -1;
        var token =await module.exports.parseAuthorization(authorization);
        if(token!== null)
        {
          try{
              var jwtToken = await jwt.verify(token,JWT_SIGN_SECRET);
              if(jwtToken!==null){
                pseudo=jwtToken.userPseudo;
          }
          }catch(err) { }
        }
        return pseudo;
      },
      getUserIdFromRenew:  async function(authorization) {
          var userId = -1;
          var token =  await module.exports.parseAuthorization(authorization);
          if(token != null) {
            try {
              var jwtToken =await jwt.verify(token, renew_jwt_sign);
              if(jwtToken != null)
                userId = jwtToken.userID;
            } catch(err) { }
          }
          return userId;
        },
      getPseudoFromRenew: async function(authorization){
          var pseudo = -1;
          var token =await module.exports.parseAuthorization(authorization);
          if(token!== null)
          {
            try{
                var jwtToken = await jwt.verify(token,renew_jwt_sign);
                if(jwtToken!==null){
                  pseudo=jwtToken.userPseudo;
            }
            }catch(err) { }
          }
          return pseudo;
        }
};